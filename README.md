# websocket-client

WebSocket del lado del cliente. Permite establecer la url del servidor websocket.

## Conceptos a desarrollar

WebSocket

## Tecnologías

Lenguaje: Javascript

## Uso

Cargar los datos del servidor:

```bash
1) Seleccionar el protocolo ws o wss
2) Ingresar el hostname
3) Ingresar el puerto
4) Ingresar el endpoint
5) Presionar el boton conectar
```
Enviar mensajes al servidor

```bash
1) Cargar un id de producto
2) Cargar una cantidad
3) Presionar el botón enviar
```
## Server ejemplo
[websocket-server-go-gorilla](https://gitlab.com/projects-go/websocket-server-go-gorilla)

[websocket-server-go-melody](https://gitlab.com/projects-go/websocket-server)

## Logs
En la sección WebSocket events se observan los distintos eventos que se generan durante la comunicación con el servidor

## Referencias
[alexyslozada](https://github.com/alexyslozada/ws-dashboard)

[pegaxchange](https://www.pegaxchange.com/2018/03/23/websocket-client/)
