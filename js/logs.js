logs = document.getElementById('logs');

function createSpan(type, label) {
    var span = document.createElement('span');
    span.style.wordWrap = 'break-word';
    switch (type) {
        case 'success':
            span.style.color = 'green';
            break;
        case 'error':
            span.style.color = 'red';
            break;
    }
    span.innerHTML = '<strong>'+label+':</strong> ';
    return span;
}

function logSuccess(label, text) {
    span = createSpan('success', label);
    var text = document.createTextNode(text);
    span.appendChild(text);
    logToScreen(span);
}
  
function logError(label, text) {
    span = createSpan('error', label);
    var text = document.createTextNode(text);
    span.appendChild(text);
    logToScreen(span);
}
  
function logToScreen(element) {
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.appendChild(element);
    logs.appendChild(p);
    logs.scrollTop = logs.scrollHeight;
}