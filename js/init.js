var wsUri = document.getElementById('wsUri');

connect = document.getElementById('connect');
disconnect = document.getElementById('disconnect');
send = document.getElementById('send');

function handlePageLoad() {
  document.getElementById('notifySupport').style.display = 'block';
  if (window.WebSocket) {
    document.getElementById('support').style.display = 'block';
  } else if (window.MozWebSocket) {
    logError('Info', 'This browser supports WebSocket using the MozWebSocket constructor');
    window.WebSocket = window.MozWebSocket;
    document.getElementById('support').style.display = 'block';
  } else {
    document.getElementById('noSupport').style.display = 'block';
  }
  wsUri = document.getElementById('wsUri');
  setGuiConnected(false);  
}
  
function setGuiConnected(isConnected) {
  protocol.disabled = isConnected;
  hostname.disabled = isConnected;
  port.disabled = isConnected;
  endpoint.disabled = isConnected;
  connect.disabled = isConnected;
  disconnect.disabled = !isConnected;
  product.disabled = !isConnected;
  quantity.disabled = !isConnected;
  send.disabled = !isConnected;
  if (isConnected) {
    labelColor = '#999999';
  }  
}

window.addEventListener('load', handlePageLoad, false);