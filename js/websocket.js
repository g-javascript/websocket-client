var websocket   = null;
var ws_protocol = null;
var ws_hostname = null;
var ws_port     = null;
var ws_endpoint = null;

function doConnect() {
    var ws_protocol = document.getElementById("protocol").value;
    var ws_hostname = document.getElementById("hostname").value;
    var ws_port     = document.getElementById("port").value;
    var ws_endpoint = document.getElementById("endpoint").value;
    webSocketURL = ws_protocol + "://" + ws_hostname + ":" + ws_port + ws_endpoint;
    websocket = new WebSocket(webSocketURL);
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}

function doDisconnect() {
    websocket.close()
}
  
function onOpen(evt) {
    logSuccess('ONOPEN', 'Servidor > Cliente: Conectado');
    setGuiConnected(true);
    // For convenience, put the cursor in the message field, and at the end of the text.
    sendMessage.focus();
    sendMessage.selectionStart = sendMessage.selectionEnd = sendMessage.value.length;
};
  
function onError(evt) {
    logError('ONERROR', 'Servidor > Cliente: Error ' + evt.data);
};
  
function onMessage(evt) {
    logSuccess('ONMESSAGE', 'Servidor > Cliente: Mensaje ' + evt.data);
    const msg = JSON.parse(evt.data);
    update(msg.product, msg.quantity);
};
  
function onClose() {
    logSuccess('ONCLOSE', 'Servidor > Cliente: Desconectado');
    setGuiConnected(false);
};

send.addEventListener('click', e => {
    e.preventDefault();
    const msg = {
        product: parseInt(product.value, 10),
        quantity: parseInt(quantity.value, 10)
    };
    logSuccess('Send', 'Cliente > Servidor: Envio de datos ' + JSON.stringify(msg));
    websocket.send(JSON.stringify(msg));
})

function doSend() {
    logTextToConsole('SENT: ' + sendMessage.value);
    websocket.send(sendMessage.value);
  }
  
disconnect.addEventListener('click', e => {
    e.preventDefault();
    logSuccess('Close', 'Cliente > Servidor: Cierre de conexion');
    websocket.close();
})  